from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from callback import *
import json
from core.config import MARK_UP



def start():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton('🏦 Мой баланс',callback_data='balance'))
    k.add(InlineKeyboardButton('🔎 Поиск SSN',callback_data='seach'))
    k.add(InlineKeyboardButton('🗂 История',callback_data='history'))
    k.add(InlineKeyboardButton('⚠ FAQ',callback_data='rules'))
    k.add(InlineKeyboardButton('🆘 SUPPORT',callback_data='support'))
    return k

def private_room(id):
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='Назад', callback_data='mainmenu'))
    k.add(InlineKeyboardButton('Пополнить баланс', callback_data=donate_callback.new(id=id)))
    return k

def start_admin():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton('🏦 Мой баланс',callback_data='balance'))
    k.add(InlineKeyboardButton('🔎 Поиск SSN',callback_data='seach'))
    k.add(InlineKeyboardButton('🗂 История',callback_data='history'))
    k.add(InlineKeyboardButton('⚠ FAQ',callback_data='rules'))
    k.add(InlineKeyboardButton('🆘 SUPPORT',callback_data='support'))
    k.add(InlineKeyboardButton('⚜ Админ панель', callback_data='admin_panel'))
    return k

def admin():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton('👥 Пользователи', callback_data='users'))
    k.add(InlineKeyboardButton('📈 Статистика', callback_data='statistik'))
    k.add(InlineKeyboardButton('💰 Баланс аккаунта',callback_data='acc_balanse'))
    k.add(InlineKeyboardButton("📤 Рассылка",callback_data='newsletter'))
    k.add(InlineKeyboardButton(text='Назад', callback_data='mainmenu'))
    return k

def account():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='Пополнить баланс', callback_data='donate'))
    k.add(InlineKeyboardButton(text='Назад', callback_data='mainmenu'))
    return k

def donate():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='Криптовалюта', callback_data='bitcoin'))
    #k.add(InlineKeyboardButton(text='Qiwi', callback_data='qiwi'))
    k.add(InlineKeyboardButton(text='Назад', callback_data='mainmenu'))
    return k
    
def back_mainmenu():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='В главное меню', callback_data='mainmenu'))
    return k

def pay_btn(url):
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(url=url,text='Оплатить'))
    k.add(InlineKeyboardButton(text='В главное меню', callback_data='mainmenu'))
    return k

def seach_type():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton('🗺 Штату',callback_data='by_state'))
    k.add(InlineKeyboardButton('🏙 Городу', callback_data='by_city'))
    k.add(InlineKeyboardButton('🆔 ZIP', callback_data='by_zip'))
    k.add(InlineKeyboardButton('🎂 DOB', callback_data='by_dob'))
    k.add(InlineKeyboardButton(text='Назад', callback_data='mainmenu'))
    return k

def states_kb():
    k = InlineKeyboardMarkup()
    with open('states.json','r') as r:
        json_states = json.loads(r.read())
    states = json_states['data']
    i = 0
    while i < len(states)-1:
        k.add(InlineKeyboardButton(states[i]['name'],callback_data=choose_state.new(name=states[i]['name'],value=states[i]['value'])),
              InlineKeyboardButton(states[i + 1]['name'],callback_data=choose_state.new(name=states[i + 1]['name'],value=states[i + 1]['value'])),
              InlineKeyboardButton(states[i + 2]['name'],callback_data=choose_state.new(name=states[i + 2]['name'],value=states[i + 2]['value'])))
        i+=3
    k.add(InlineKeyboardButton(states[-1]['name'],callback_data=choose_state.new(name=states[-1]['name'],value=states[-1]['value'])))
    k.add(InlineKeyboardButton(text='В главное меню', callback_data='mainmenu'))
    return k

def buy(hid,rid):
    k = InlineKeyboardMarkup()
    price = round(4.5 * MARK_UP,2)
    k.add(InlineKeyboardButton('Купить '+str(price),callback_data=buy_callback.new(hid=hid,rid=rid,price=str(price))))
    return k

def end_search():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='В главное меню', callback_data='mainmenu'))
    return k

def search_user():
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='По нику', callback_data='for_nickname'),
          InlineKeyboardButton(text='По ID', callback_data='for_id'))
    k.add(InlineKeyboardButton(text='Назад', callback_data='back_admin'))
    return k

def user_menu(id):
    k = InlineKeyboardMarkup()
    k.add(InlineKeyboardButton(text='Изменить баланс',callback_data=change_balance.new(str(id))))
    k.add(InlineKeyboardButton(text='Назад', callback_data='back_admin'))
    return k