import sqlalchemy
from .base import metadata

pays = sqlalchemy.Table(
    'pays',
    metadata,
    sqlalchemy.Column('id',sqlalchemy.Integer,primary_key=True,autoincrement=True,unique=True),
    sqlalchemy.Column('buyer',sqlalchemy.ForeignKey('users.id_user')),
    sqlalchemy.Column('ssn',sqlalchemy.String),
    sqlalchemy.Column('name',sqlalchemy.String),
    sqlalchemy.Column('address',sqlalchemy.String),
    sqlalchemy.Column('dob',sqlalchemy.String),
    sqlalchemy.Column('dob_year',sqlalchemy.String),
    sqlalchemy.Column('origin_price',sqlalchemy.Float),
    sqlalchemy.Column('res_balance',sqlalchemy.Float),
    sqlalchemy.Column('date',sqlalchemy.Date)
)