from .users import users
from .payments import payments
from .pays import pays
from .base import metadata,engine


metadata.create_all(bind=engine)