import sqlalchemy
from .base import metadata
import datetime

users = sqlalchemy.Table(
    'users',
    metadata,
    sqlalchemy.Column('id',sqlalchemy.Integer,primary_key=True,autoincrement=True,unique=True),
    sqlalchemy.Column('id_user',sqlalchemy.Integer,unique=True),
    sqlalchemy.Column('nickname',sqlalchemy.String,unique=True,nullable=True),
    sqlalchemy.Column('balance',sqlalchemy.FLOAT,default=0),
    sqlalchemy.Column('referral',sqlalchemy.Integer,nullable=True),
    sqlalchemy.Column('status',sqlalchemy.String),
    sqlalchemy.Column('lang',sqlalchemy.String),
    sqlalchemy.Column('date_reg',sqlalchemy.Date))