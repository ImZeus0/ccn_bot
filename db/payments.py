import sqlalchemy
from .base import metadata
import datetime

payments = sqlalchemy.Table('payments',
                            metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True, autoincrement=True,unique=True),
    sqlalchemy.Column('merchant',sqlalchemy.String),
    sqlalchemy.Column('invoice_id',sqlalchemy.String,unique=True),
    sqlalchemy.Column('invoice_amount',sqlalchemy.Float),
    sqlalchemy.Column('invoice_currency',sqlalchemy.String),
    sqlalchemy.Column('invoice_status',sqlalchemy.String),
    sqlalchemy.Column('order_id',sqlalchemy.String),
    sqlalchemy.Column('date_time',sqlalchemy.String))