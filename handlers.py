"""
@dp.callback_query_handler(text_contains="back_to_menu")
async def privateroom(call:CallbackQuery):
    conn = connect()
    lang = get_lang(conn, call.message.chat.id)
    conn.close()
    await call.message.edit_text('Main menu')
    await call.message.edit_reply_markup(start(lang))



@dp.callback_query_handler(text_contains="bitcoin")
async def gonatecrypto(call:CallbackQuery):
    conn = connect()
    lang = get_lang(conn, call.message.chat.id)
    conn.close()
    await call.message.edit_text(paste('enter_pay',lang))
    await call.message.edit_reply_markup(check_payment(lang))
    await Donate.enter_ammount.set()

@dp.message_handler(state=Donate.enter_ammount)
async def enterdonate(m:Message,state:FSMContext):
    conn = connect()
    lang = get_lang(conn, m.chat.id)
    conn.close()
    try :
        ammount = float(m.text)
        word = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(12)])
        id = str(m.chat.id) + word
        log(f"{datetime.datetime.now()} [+] Create order id:{id} amount:{m.text} user:{m.chat.username}")
        url = pay.startpayment(ammount, id)
        await m.answer('Нажмите чтобы оплатить', reply_markup=pay_btn(url,lang))
    except Exception as e :
        await m.answer(paste('invalid', lang)+str(e), reply_markup=start(lang))
    await state.finish()

@dp.callback_query_handler(text_contains="qiwi")
async def gonateqiwi(call:CallbackQuery):
    conn = connect()
    lang = get_lang(conn, call.message.chat.id)
    conn.close()
    await call.message.edit_text(f'{paste("help",lang)} @CRASH_adm ')
    await call.message.edit_reply_markup(check_payment(lang))

    

@dp.callback_query_handler(text_contains="seach")
async def startsearch(call:CallbackQuery,state :FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('choose_reg',lang))
    await call.message.edit_reply_markup(choose_country(lang))

@dp.callback_query_handler(country_callback.filter())
async def searchcountry(call:CallbackQuery,state :FSMContext,callback_data : dict):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    country = callback_data.get('name')
    await state.update_data(country=country)
    await call.message.edit_text(paste('country', lang))
    await call.message.edit_reply_markup(choose_region(country, lang))
    await Serch.type_ip.set()

@dp.callback_query_handler(region_callback.filter(),state=Serch.type_ip)
async def searchtyprip(call:CallbackQuery,state :FSMContext,callback_data : dict):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    data = await state.get_data()
    region = callback_data.get('name')
    await state.update_data(region=region)
    await call.message.edit_text(paste('choose_type', lang))
    await call.message.edit_reply_markup(choose_type_ip(lang,data['country']))
    await Serch.next()

@dp.callback_query_handler(text_contains = 'backregion',state='*')
async def backcountry(call:CallbackQuery,state :FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    country = call.data.split('_')[1]
    await state.update_data(country=country)
    await call.message.edit_text(paste('country', lang))
    await call.message.edit_reply_markup(choose_region(country, lang))
    await Serch.previous()

@dp.callback_query_handler(type_ip_callback.filter(),state=Serch.type_blacklists)
async def searchtypeblack(call:CallbackQuery,state :FSMContext,callback_data : dict):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    type_ip = callback_data.get('name')
    await state.update_data(type_ip=type_ip)
    await call.message.edit_text(paste('choose_type', lang)+" Blacklists")
    await call.message.edit_reply_markup(choose_blacklists(lang))
    await Serch.next()


@dp.callback_query_handler(type_blacklist_callback.filter(),state=Serch.city)
async def searchcityall(call:CallbackQuery,state :FSMContext,callback_data : dict):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    type_blacklists = callback_data.get('name')
    await state.update_data(type_blacklists=type_blacklists)
    await call.message.edit_text(paste('input_city', lang))
    await call.message.edit_reply_markup(choose_city(lang))
    await Serch.next()

@dp.message_handler(state=Serch.zip)
async def searchcity(m:Message,state :FSMContext):
    city = m.text
    c = connect()
    lang = get_lang(c, m.chat.id)
    c.close()
    await state.update_data(city=city)
    await m.answer(paste('input_zip',lang),reply_markup=choose_zip(lang))
    await Serch.next()

@dp.callback_query_handler(text_contains='backip',state='*')
async def backips(call:CallbackQuery,state:FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    data = await state.get_data()
    try:
        await call.message.edit_text(paste('choose_type', lang))
        await call.message.edit_reply_markup(choose_type_ip(lang,data['country']))
    except KeyError :
        await call.message.edit_text(paste('main', lang))
        await call.message.edit_reply_markup(start(lang))
    await state.finish()

@dp.callback_query_handler(text_contains='all_city',state=Serch.zip)
async def searczipall(call:CallbackQuery,state :FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    await state.update_data(city='all')
    await call.message.edit_text(paste('input_zip', lang))
    await call.message.edit_reply_markup(reply_markup=choose_zip(lang))
    await Serch.next()


@dp.callback_query_handler(text_contains='backcity',state='*')
async def backblack(call:CallbackQuery,state:FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('choose_type', lang) + " Blacklists")
    await call.message.edit_reply_markup(choose_blacklists(lang))
    await Serch.previous()

@dp.callback_query_handler(text_contains='all_zip',state=Serch.end_search)
async def endserach(call:CallbackQuery,state : FSMContext):
    c = connect()
    id = call.message.chat.id
    lang = get_lang(c, call.message.chat.id)
    balance = float(get_balanse_s(c, id))
    c.close()
    await state.update_data(zip='all')
    data = await state.get_data()
    await state.finish()
    if balance > 0:
        await call.message.edit_text(paste('wait', lang))
        await call.message.edit_reply_markup(check_payment(lang))
        driver = Pars()
        proxys = driver.seach_proxy(id, data)
        await send_proxy(proxys, id)
    else:
        await call.message.edit_text("Для поиска нужно чтоб на балансе был минимум 1$")
        await call.message.edit_reply_markup(reply_markup=account(lang))

@dp.callback_query_handler(text_contains='backzip',state='*')
async def backzip(call:CallbackQuery,state:FSMContext):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('input_city', lang))
    await call.message.edit_reply_markup(choose_city(lang))
    await Serch.previous()

@dp.message_handler(state=Serch.end_search)
async def endserach(m:Message,state : FSMContext):
    await state.update_data(zip=m.text)
    data = await state.get_data()
    await state.finish()
    id = m.chat.id
    c = connect()
    lang = get_lang(c,id)
    balance = float(get_balanse_s(c,id))
    c.close()
    if balance > 0:
        await m.answer(paste('wait',lang))
        driver = Pars()
        proxys = driver.seach_proxy(id, data)
        await send_proxy(proxys, id)
    else:
        await m.edit_text("Для поиска нужно чтоб на балансе был минимум 1$")
        await m.edit_reply_markup(reply_markup=account(lang))
#_____________________________________________________________________
async def send_proxy(list_proxy, id):
    c = connect()
    lang = get_lang(c, id)
    if list_proxy != None:
        if len(list_proxy) >= 10:
            for i in range(10):
               await create_proxy_msg(list_proxy[i], id)
        else:
            for i in range(len(list_proxy)):
               await create_proxy_msg(list_proxy[i], id)
        await bot.send_message(id, '➖➖➖➖➖➖➖➖➖', reply_markup=check_payment(lang))
    else:
        await bot.send_message(id, paste('no_res',lang), reply_markup=check_payment(lang))
    c.close()

async def create_proxy_msg(proxy,id):
    msg = f'<b>IP</b> {proxy.ip}\n<b>DOMAIN</b> {proxy.domain}' \
          f'\n<b>TYPE</b> {proxy.type}\n<b>CITY</b> {proxy.city}' \
          f'\n<b>SPEED</b> {proxy.speed}\n<b>ZIP</b> {proxy.zip}\n<b>PING</b> {proxy.ping}'
    await bot.send_message(id, msg, parse_mode='HTML', reply_markup=btn_buy(proxy.id_btn, proxy.price, proxy.out_price))


@dp.callback_query_handler(text_contains="history")
async def hist(call:CallbackQuery):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    proxys = select_order(c, call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('history',lang))
    await call.message.edit_reply_markup(history_proxy(proxys))

@dp.callback_query_handler(hist_proxy.filter())
async def infohist(call:CallbackQuery,callback_data:dict):
    ip_port = callback_data.get('proxy').split('/')
    prolong = False
    info_proxy = select_proxy(ip_port[0]+':'+ip_port[1])
    msg = f'IP:PORT {info_proxy["proxy"]}\n🕑 {info_proxy["date"]}\n💲 {info_proxy["ammount"]}\n🏙 {info_proxy["city"]}\nzip {info_proxy["zip"]}'
    date = info_proxy["date"].split(' ')[0].split('-')
    time = info_proxy["date"].split(' ')[1].split(':')
    buy_date = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]))
    res = datetime.datetime.now() - buy_date
    c = connect()
    lang = get_lang(c,call.message.chat.id)
    c.close()
    print(res)
    if res > datetime.timedelta(days=1):
        prolong = True
    await call.message.edit_text(msg)
    await call.message.edit_reply_markup(prolong_kb(info_proxy["proxy"],info_proxy["ammount"],lang,prolong))

@dp.callback_query_handler(text_contains="backhist")
async def hist(call:CallbackQuery):
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    proxys = select_order(c, call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('history', lang))
    await call.message.edit_reply_markup(history_proxy(proxys))


@dp.callback_query_handler(text_contains="prolong")
async def prolong_proxy(call:CallbackQuery):
    await call.message.edit_text('Идет процесс продления ...')
    proxy = call.data.split('_')[1]
    id = call.message.chat.id
    d = Pars()
    c = connect()
    lang = get_lang(c,id)
    info_proxy = select_proxy(proxy)
    ammount = float(info_proxy["ammount"])
    url = 'https://luxsocks.ru/login'
    account = d.choose_account()
    d.login_page(id, url,account)
    msg = d.prolong(proxy)
    if msg == 'OK':
        reduce_funds(c, id, ammount)
        balance = float(get_balanse_s(c, id))
        add_prolong(c, id, proxy, ammount, balance)
        await call.message.edit_text('Прокси <code>'+proxy+'</code> продлен успешно')
        await call.message.edit_reply_markup(check_payment(lang))
        log(str(datetime.datetime.now())+'[+] PROLONG OK id_user'+str(call.message.chat.id)+' '+proxy+' '+str(ammount)+'$ balance: '+str(balance))
    elif msg.startswith('ERROR'):
        await call.message.edit_text('Прокси <code>' + proxy + '</code> невозможно продлить')
        await call.message.edit_reply_markup(check_payment(lang))
        log(str(datetime.datetime.now())+'[x] '+msg+' id_user: '+str(call.message.chat.id))
    elif msg == 'JSON_ERROR':
        await call.message.edit_text('Прокси <code>' + proxy + '</code> невозможно продлить')
        await call.message.edit_reply_markup(check_payment(lang))
        log(str(datetime.datetime.now())+'[x] '+msg+' id_user: '+str(call.message.chat.id))


async def buy_proxy(id, id_btn):
    d = Pars()
    url = 'https://luxsocks.ru/login'
    account = d.choose_account()
    print("SELECT: ",account)
    d.login_page(id,url,account)
    zip = d.get_displaymodal(id_btn, 'zip')
    try:
        amount = float(d.get_displaymodal(id_btn, 'rent_price')) * config.RENT_PRICE
        origin_price = float(d.get_displaymodal(id_btn, 'rent_price'))
    except:
        pass
    date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    city = d.get_displaymodal(id_btn, 'city')
    region = d.get_displaymodal(id_btn, 'region')
    ping = d.get_displaymodal(id_btn, 'ping')
    type_name = d.get_displaymodal(id_btn, 'typename')
    proxy = d.buy(id_btn)
    d.driver.quit()
    c = connect()
    lang = get_lang(c, id)
    if proxy != "ERROR" and zip != "ERROR":
        try:
            reduce_funds(c, id, amount)
            balance = float(get_balanse_s(c, id))
            await bot.send_message(id,
	                     f'<b>{paste("order", lang)}</b>\n{proxy}\n<b>REGION</b>: {region}\n<b>CITY</b>: {city}\n<b>PING</b>: {ping}\n<b>TYPE_NAME</b>: {type_name}',
                             reply_markup=start(lang), parse_mode='HTML')
            add_pay(c, date_now, str(amount)[:4], id, proxy, id_btn, city, zip, region, ping, type_name,
                                  origin_price, balance, account[0])
            log(str(datetime.datetime.now()) + f"[+] {str(id)} BUY price:{str(amount)} id btn:{str(id_btn)} res balance:{str(balance)}]")
        except Exception as e:
            await bot.send_message(config.ADMIN, traceback.format_exc() + " " + str(e))
            await bot.send_message(id, paste('error_buy',lang), reply_markup=start(lang))
            log(str(
                datetime.datetime.now()) + f"[x] {str(id)}ERROR BUY id_btn:{str(id_btn)} message:{str(e)}")
    else:
        await bot.send_message(id, paste('error_buy',lang), reply_markup=start(lang))
        log(str(datetime.datetime.now()) + f"[x] {str(id)} ERROR BUY id_btn:{str(id_btn)} message: error in site")
    c.close()
    os.system('sync; echo 1 > /proc/sys/vm/drop_caches')


async def buy_out_proxy(id, id_btn):
    d = Pars()
    url = 'https://luxsocks.ru/login'
    account = d.choose_account()
    print("SELECT: ",account)
    d.login_page(id,url,account)
    time.sleep(5)
    zip = d.get_displaymodal(id_btn, 'zip')
    try:
        amount = float(d.get_displaymodal(id_btn, 'price')) * config.BUY_PRICE
        origin_price = float(d.get_displaymodal(id_btn, 'rent_price'))
    except:
        pass
    date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    city = d.get_displaymodal(id_btn, 'city')
    region = d.get_displaymodal(id_btn, 'region')
    ping = d.get_displaymodal(id_btn, 'ping')
    type_name = d.get_displaymodal(id_btn, 'typename')
    proxy = d.buy(id_btn)
    d.driver.quit()
    c = connect()
    lang = get_lang(c, id)
    if proxy != "ERROR" and zip != "ERROR":
        try:
            reduce_funds(c, id, amount)
            balance = float(get_balanse_s(c, id))
            add_pay(c, date_now, str(amount)[:4], id, proxy, id_btn, city, zip, region, ping, type_name,
                                  origin_price, balance, account[0])
            await bot.send_message(id,
	                     f'<b>{paste("order", lang)}</b>\n{proxy}\n<b>REGION</b>: {region}\n<b>CITY</b>: {city}\n<b>PING</b>: {ping}\n<b>TYPE_NAME</b>: {type_name}',
                             reply_markup=start(lang), parse_mode='HTML')
            log(str(
                datetime.datetime.now()) + f"[+] {str(id)} BUY price:{str(amount)} id btn:{str(id_btn)} res balance:{str(balance)}]")
        except Exception as e:
            await bot.send_message(config.ADMIN, traceback.format_exc() + " " + str(e))
            await bot.send_message(id, paste('erroo_buy',lang))
            await bot.send_message(id, paste('error_buy', lang), reply_markup=start(lang))
            log(str(
                datetime.datetime.now()) + f"[x] {str(id)}ERROR BUY id_btn:{str(id_btn)} message:{str(e)}")
    else:
        await bot.send_message(id, paste('error_buy',lang))
        log(str(datetime.datetime.now()) + f"[x] {str(id)} ERROR BUY id_btn:{str(id_btn)} message: error in site")
    c.close()
    os.system('sync; echo 1 > /proc/sys/vm/drop_caches')

#_____________________________________________________________________

@dp.callback_query_handler(buy_callback.filter())
async def buyproxy(call:CallbackQuery,callback_data:dict):
    price = float(callback_data.get('price'))
    id_proxy = callback_data.get('id')
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    balance_db = get_balanse_s(c, call.message.chat.id)
    balance = float(balance_db)
    if balance > price:
        try:
            await call.message.edit_text(paste('wait_order', lang))
            await buy_proxy(call.message.chat.id, id_proxy)
        except Exception as e:
            await call.message.edit_text(paste(f'{paste("error",lang)}', lang))
            await bot.send_message(config.ADMIN, traceback.format_exc() + ' ' + str(call.message.chat.id))
    else:
        await call.message.edit_text("❗Недостаточно средств❗")
        await call.message.edit_reply_markup(start(lang))
        log(str(datetime.datetime.now()) + f"[x] {str(call.message.chat.id)} insufficient funds for rent")
    c.close()

@dp.callback_query_handler(buyout_callback.filter())
async def buyoutproxy(call:CallbackQuery,callback_data:dict):
    price = float(callback_data.get('price'))
    id_proxy = callback_data.get('id')
    c = connect()
    lang = get_lang(c, call.message.chat.id)
    balance_db = get_balanse_s(c, call.message.chat.id)
    balance = float(balance_db)
    if balance > price:
        try:
            await call.message.edit_text(paste('wait_order', lang))
            await buy_out_proxy(call.message.chat.id,id_proxy)
        except Exception as e:
            await call.message.edit_text(paste(f'{paste("error",lang)}', lang))
            await bot.send_message(config.ADMIN, traceback.format_exc() + ' ' + str(call.message.chat.id))
    else :
        await call.message.edit_text("❗Недостаточно средств❗")
        await call.message.edit_reply_markup(start(lang))
        log(str(datetime.datetime.now()) + f"[x] {str(call.message.chat.id)} insufficient funds for rent")
        c.close()

@dp.callback_query_handler(text_contains='support')
async def supportuser(call:CallbackQuery):
    c = connect()
    lang = get_lang(c,call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('admins',lang))
    await call.message.edit_reply_markup(check_payment(lang))

@dp.callback_query_handler(text_contains='rules')
async def supportuser(call:CallbackQuery):
    c = connect()
    lang = get_lang(c,call.message.chat.id)
    c.close()
    await call.message.edit_text(paste('text_rules',lang))
    await call.message.edit_reply_markup(check_payment(lang))

@dp.callback_query_handler(text_contains='acc_balanse')
async def balance(call:CallbackQuery):
    await call.message.answer("Баланс: " + str(config.BALANCE))

@dp.callback_query_handler(text_contains='newsletter')
async def let(call:CallbackQuery,state:FSMContext):
    c = connect()
    lang = get_lang(c,call.message.chat.id)
    c.close()
    await call.message.answer('Введите текст',reply_markup=check_payment(lang))
    await Letters.enter_text.set()

@dp.message_handler(state=Letters.enter_text)
async def send(m: Message,state:FSMContext):
    msg = m.text
    conn = connect()
    users = get_confirmed_users(conn)
    for user in users:
        try:
            await bot.send_message(user['id_user'], msg)
        except Exception as e:
            pass
    await m.answer('Рассылка закончилась')
    await state.finish()
"""