from aiogram.utils.callback_data import CallbackData

lang_callback = CallbackData('lang','la')
donate_callback = CallbackData('donate','id')
choose_state = CallbackData('state','value','name')
buy_callback = CallbackData('buy','hid','rid','price')
change_balance = CallbackData('change_b','id_user')