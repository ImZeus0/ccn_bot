import aiohttp
from core.config import BASE_URL_API


async def get(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as responce:
            if responce.status == 200:
                return await responce.json()
            else:
                return {'error': '3', 'msg': 'response server ' + str(responce.status)}


async def seach(type, f_name, l_name, value):
    url = BASE_URL_API + '/search'
    url += f"?type={type}&f_name={f_name}&l_name={l_name}&value={value}"
    return await get(url)

async def buy(hid,rid):
    url = BASE_URL_API + f'/buy?hid={hid}&rid={rid}'
    return await get(url)

async def balance():
    url = BASE_URL_API + f'/balance'
    return await get(url)