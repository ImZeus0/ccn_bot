import logging

from loader import bot, storage

from db.base import database

async def on_startup(dp):
    await database.connect()


async def on_shutdown(dp):
    await bot.close()
    await database.disconnect()
    await storage.close()


if __name__ == '__main__':
    from aiogram import executor
    from handlers import dp


    executor.start_polling(dp,on_startup=on_startup, on_shutdown=on_shutdown)