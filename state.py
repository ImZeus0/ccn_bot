from aiogram.dispatcher.filters.state import StatesGroup, State


class Input(StatesGroup):
    first_name = State()
    last_name = State()
    city = State()
    dob = State()
    zip = State()

class Donate(StatesGroup):
    enter_ammount = State()
    set_balance = State()

class Letters(StatesGroup):
    enter_text = State()

class Search_user(StatesGroup):
    nickname = State()
    id = State()