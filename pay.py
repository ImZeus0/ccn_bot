import hashlib
import requests
from core import config


def check_pay(invoice_id):
    url = 'https://api.cryptonator.com/api/merchant/v1/getinvoice?'
    params = config.MERCHANT_ID + '&' + invoice_id + '&' + config.SECRET_KEY
    hash = hashlib.sha1()
    hash.update(bytes(params, 'utf-8'))
    print(type(hash.hexdigest()))
    res = url +'merchant_id=' + config.MERCHANT_ID + '&invoice_id=' + invoice_id + '&secret_hash=' + hash.hexdigest()
    print(res)
    response = requests.get(res)
    data = response.content
    print(data)

def startpayment(amount,id):
    start_url = 'https://api.cryptonator.com/api/merchant/v1/startpayment?'
    item_name = '&item_name=proxy'
    invoice_amount = '&invoice_amount='+str(amount)
    invoice_currency = '&invoice_currency=usd'
    merchant = '&merchant_id=' + config.MERCHANT_ID
    order_id = '&order_id='+str(id)
    return start_url+merchant+item_name+invoice_amount+invoice_currency+order_id

