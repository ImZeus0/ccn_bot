from pydantic import BaseModel
from typing import Optional
from fastapi import Form
from core.config import ORIGIN_PRICE
from datetime import datetime
from .user import User



class Order(BaseModel):
    id: Optional[str] = None
    date: datetime = datetime.now()
    buyer: int
    ssn: str
    name: str
    address: str
    dob: str
    dob_year: str
    origin_price: float = ORIGIN_PRICE
    res_balance: float



class Confirm(BaseModel):
    status: int
    entry: Order

class ReceptionPayCard(BaseModel):
    invoice_id: str
    order_id: str
    date_time : str
    invoice_amount: float
    invoice_status: str

class ReceptionPay(BaseModel):
    id: Optional[int] = None
    merchant_id: Optional[str] = None
    invoice_id: Optional[str] = None
    invoice_created: Optional[int] = None
    invoice_expires: Optional[int] = None
    invoice_amount:  Optional[float] = None
    invoice_currency: Optional[str] = None
    invoice_status: Optional[str] = None
    invoice_url: Optional[str] = None
    order_id: Optional[str] = None
    checkout_address: Optional[str] = None
    checkout_amount: Optional[float] = None
    checkout_currency : Optional[str] = None
    date_time : Optional[str] = None
    secret_hash: Optional[str] = None

    @classmethod
    def as_form(cls, id: Optional[int] = Form(None), merchant_id: str = Form(...),
                invoice_id: str = Form(...),
                invoice_created: int = Form(...),
                invoice_expires: int = Form(...),
                invoice_amount: float = Form(...),
                invoice_currency: str = Form(...),
                invoice_status: str = Form(...),
                invoice_url: str = Form(...),
                order_id: str = Form(...),
                checkout_address: str = Form(...),
                checkout_amount: float = Form(...),
                checkout_currency: str = Form(...),
                date_time: str = Form(...),
                secret_hash: str = Form(...)) -> 'ReceptionPay':
        return cls(id=id,
                   merchant_id=merchant_id,
                   invoice_id=invoice_id,
                   invoice_created=invoice_created,
                   invoice_expires=invoice_expires,
                   invoice_amount=invoice_amount,
                   invoice_currency=invoice_currency,
                   invoice_status=invoice_status,
                   invoice_url=invoice_url,
                   order_id=order_id,
                   checkout_address=checkout_address,
                   checkout_amount=checkout_amount,
                   checkout_currency=checkout_currency,
                   date_time=date_time,
                   secret_hash=secret_hash)


