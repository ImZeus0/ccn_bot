from pydantic import BaseModel
from typing import Optional
import datetime

class User(BaseModel):
    id_user : int
    balance : float = 0
    nickname : Optional[str] = None
    referral : Optional[int] = None
    date_reg: Optional[datetime.date]
    status : str = 'user'
    lang: str = 'ru'



