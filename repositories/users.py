from .base import BaseRepository
from db.users import users
from typing import List, Optional
from models.user import User
from datetime import datetime


class UserRepository(BaseRepository):

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[User]:
        query = users.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query)

    async def get_by_id(self, id: int) -> Optional[User]:
        query = users.select().where(users.c.id_user == id)
        user = await self.database.fetch_one(query)
        if user is None:
            return None
        return User.parse_obj(user)

    async def get_referrals(self, id) -> List[User]:
        query = users.select().where(users.c.referral == id)
        return await self.database.fetch_all(query)

    async def get_by_nickname(self, nickname: str) -> Optional[User]:
        query = users.select().where(users.c.nickname == nickname)
        user = await self.database.fetch_one(query)
        if user is None:
            return None
        return User.parse_obj(user)

    async def is_register(self, id_user):
        query = users.select().where(users.c.id_user == id_user)
        user = await self.database.fetch_one(query)
        if user is None:
            return False
        return True

    async def is_admin(self, id_user):
        query = users.select().where(users.c.id_user == id_user)
        user = await self.database.fetch_one(query)

        if user is None:
            return False

        if user['status'] == 'admin':
            return True
        return False


    async def create(self, id_user, nickname, referral):
        user = User(

            id_user=id_user,
            nickname=nickname,
            referral=referral,
            date_reg=datetime.now()
        )
        values = {**user.dict()}
        query = users.insert().values(**values)
        await self.database.execute(query)
        return user

    async def update_status(self, id: int, status: str) -> Optional[User]:
        values = {'status': status}
        query = users.update().where(users.c.id_user == id).values(**values)
        await self.database.execute(query)
        query = users.select().where(users.c.id_user == id)
        user = await self.database.fetch_one(query)
        if user is None:
            return {'error': 12, 'msg': 'User not found'}
        return User.parse_obj(user)

    async def update_lang(self, id: int, lang: str) -> Optional[User]:
        values = {'lang': lang}
        query = users.update().where(users.c.id_user == id).values(**values)
        await self.database.execute(query)
        query = users.select().where(users.c.id_user == id)
        user = await self.database.fetch_one(query)
        if user is None:
            return {'error': 12, 'msg': 'User not found'}
        return User.parse_obj(user)

    async def update_balance_by_id(self, id: int, balance: float) -> Optional[User]:
        values = {'balance': balance}
        query = users.update().where(users.c.id_user == id).values(**values)
        await self.database.execute(query)
        query = users.select().where(users.c.id_user == id)
        user = await self.database.fetch_one(query)
        if user is None:
            return {'error': 12, 'msg': 'User not found'}
        return User.parse_obj(user)
