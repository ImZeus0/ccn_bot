from models.orders import   ReceptionPay,ReceptionPayCard
from datetime import datetime
from .base import BaseRepository
from db.payments import payments
from typing import List, Optional


class PaymentsRepository(BaseRepository):

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[ReceptionPay]:
        query = payments.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query)

    async def get_by_id(self, id: int) -> Optional[ReceptionPay]:
        query = payments.select().where(payments.c.id == id)
        pay = await self.database.fetch_one(query)
        if pay is None:
            return None
        return ReceptionPay.parse_obj(pay)

    async def get_all_payments_for_day(self):
        now_date = datetime.now().strftime('20%y-%m-%d')
        print(now_date)
        query = payments.select().where(payments.c.date_time.startswith(now_date))
        return await self.database.fetch_all(query)

    async def get_by_id_user(self, id: str) -> List[ReceptionPay]:
        query = payments.select().where(payments.c.order_id.startswith(id))
        return await self.database.fetch_all(query)

    async def create(self,p:ReceptionPay):
        values = {**p.dict()}
        values.pop('id',None)
        values['invoice_amount'] = float(values['invoice_amount'])
        query = payments.insert().values(values)
        payments.id = await self.database.execute(query)
        data = {}
        data['status'] = 1
        return data

    async def create_card(self,p:ReceptionPayCard):
        values = {**p.dict()}
        query = payments.insert().values(values)
        payments.id = await self.database.execute(query)
        data = {}
        data['status'] = 1
        return data
