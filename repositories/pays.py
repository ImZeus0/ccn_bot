

from models.orders import Order
from .base import BaseRepository
from db.pays import pays
from typing import List, Optional
from models.user import User


class PayRepository(BaseRepository):
    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Order]:
        query = pays.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query)

    async def get_by_id(self, id: int) -> Optional[Order]:
        query = pays.select().where(pays.c.id == id)
        pay = await self.database.fetch_one(query)
        if pay is None:
            return None
        return Order.parse_obj(pay)

    async def get_by_buyer(self, buyer: User) -> Optional[List[Order]]:
        query = pays.select().where(pays.c.buyer == buyer).limit(2)
        res = await self.database.fetch_all(query)
        if res is None:
            return None
        return res


    async def create_pay(self,pay:Order):
        values = {**pay.dict()}
        values.pop('id', None)
        query = pays.insert().values(**values)
        pay.id = await self.database.execute(query)
        return 0
