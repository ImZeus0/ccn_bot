from loader import bot, dp
from aiogram.types import Message
from aiogram.dispatcher import FSMContext
import random
import pay
from keyboards import *
from repositories.pays import PayRepository
from state import *
from db.base import database
from repositories.users import UserRepository
from aiogram.types.callback_query import CallbackQuery


@dp.callback_query_handler(text_contains="balance")
async def privateroom(call: CallbackQuery):
    users = UserRepository(database)
    id = call.message.chat.id
    user = await users.get_by_id(id)
    bot_username = (await bot.get_me()).username
    bot_link = 'http://t.me/{bot_name}?start={id_user}'.format(bot_name=bot_username, id_user=id)
    referrals = await users.get_referrals(id)
    msg = f"🆔 <code>{str(id)}</code> \n" \
          f"💰 <b>Баланс</b> <code>{str(user.balance)}</code>\n" \
          f"🕘 <b>Дата регистрации</b> <code>{user.date_reg.isoformat()}</code>\n" \
          f"<b>Ссилка для рефералов</b> " + bot_link + "\n" \
                                                       f"<b>Количество рефералов</b> <code>{str(len(referrals))}</code> \n"
    await call.message.edit_text(msg)
    await call.message.edit_reply_markup(private_room(id))


@dp.callback_query_handler(donate_callback.filter())
async def gonate(call: CallbackQuery):
    await call.message.edit_text('Выберите тип оплаты')
    await call.message.edit_reply_markup(donate())


@dp.callback_query_handler(text_contains="bitcoin")
async def gonatecrypto(call:CallbackQuery):
    await call.message.edit_text("Введите сумму пополнения")
    await call.message.edit_reply_markup(back_mainmenu())
    await Donate.enter_ammount.set()


@dp.message_handler(state=Donate.enter_ammount)
async def enterdonate(m:Message,state:FSMContext):
    try :
        ammount = float(m.text)
        word = ''.join([random.choice(list('123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')) for x in range(12)])
        id = str(m.chat.id) + word
        url = pay.startpayment(ammount, id)
        await m.answer('Нажмите чтобы оплатить', reply_markup=pay_btn(url))
    except Exception as e :
        await m.answer("Неверно введена сумма\nпопробуйте еще раз", reply_markup=start())
    await state.finish()

@dp.callback_query_handler(text_contains='history')
async def history(call:CallbackQuery):
    pays = PayRepository(database)
    pays = await pays.get_by_buyer(call.message.chat.id)
    for pay in pays:
        text = '<b>🔹 SSN</b> ' + pay['ssn'] + '\n'
        text += '<b>🔹 NAME</b> ' + pay['name'] + '\n'
        text += '<b>🔹 ADDRESS</b> ' + pay['address'] + '\n'
        text += '<b>🔹 DOB</b> ' + pay['dob'] + '\n'
        text += '<b>🔹 DOB YEAR</b> ' + pay['dob_year'] + '\n'
        await bot.send_message(call.message.chat.id,text)
