from loader import bot, dp
from aiogram.types import Message
from aiogram.dispatcher import FSMContext
from keyboards import *
from state import *
from core import api
from repositories.pays import PayRepository
from db.base import database
from repositories.users import UserRepository
from aiogram.types.callback_query import CallbackQuery

@dp.callback_query_handler(text_contains='admin_panel')
async def get_admin_panel(call:CallbackQuery):
    await call.message.edit_text('Админ панель')
    await call.message.edit_reply_markup(admin())

@dp.callback_query_handler(text_contains='back_admin')
async def back_admin_metod(call:CallbackQuery):
    await call.message.edit_text('Админ панель')
    await call.message.edit_reply_markup(admin())


@dp.callback_query_handler(text_contains='acc_balanse')
async def get_balanse_metod(call:CallbackQuery):
    await call.message.answer('Обновление ...')
    balance = await api.balance()
    await call.message.answer('Баланс: <code>'+str(balance)+"</code>")

@dp.callback_query_handler(text_contains='newsletter')
async def broadcast_metod(call:CallbackQuery,state: FSMContext):
    await call.message.answer('Введите текст', reply_markup=back_mainmenu())
    await Letters.enter_text.set()

@dp.message_handler(state=Letters.enter_text)
async def send(m: Message, state: FSMContext):
    msg = m.text
    users = UserRepository(database)
    for user in await users.get_all():
        try:
            await bot.send_message(user.id_user, msg)
        except Exception as e:
            pass
    await m.answer('Рассылка закончилась')
    await state.finish()

@dp.callback_query_handler(text_contains='users')
async def get_balanse_metod(call:CallbackQuery):
    users = UserRepository(database)
    await call.message.edit_text(f"Всего <code>{str(len(await users.get_all()))}</code> пользователей")
    await call.message.edit_reply_markup(search_user())

@dp.callback_query_handler(text_contains='for_nickname')
async def get_user_for_nickname_m(call:CallbackQuery):
    await call.message.edit_text(f'Введите ник')
    await Search_user.nickname.set()

@dp.callback_query_handler(text_contains='for_id')
async def get_user_for_id_m(call:CallbackQuery):
    await call.message.edit_text(f'Введите id')
    await Search_user.id.set()

@dp.message_handler(state=Search_user.nickname)
async def end_seach_nickname(m:Message,state:FSMContext):
    await state.finish()
    users = UserRepository(database)
    user = await users.get_by_nickname(m.text)
    if user is not None:
        msg = f"ID USER -> {str(user.id_user)}\nНИК -> {user.nickname}\nБАЛАНС ->{str(user.balance)}\nДАТА РЕГИСТРАЦИИ {user.date_reg} "
        await m.answer(msg,reply_markup=user_menu(user.id_user))
    else:
        await m.answer('Пользователь не найден',reply_markup=admin())

@dp.message_handler(state=Search_user.id)
async def end_seach_id(m:Message,state:FSMContext):
    await state.finish()
    users = UserRepository(database)
    user = await users.get_by_id(int(m.text))
    if user is not None:
        msg = f"ID USER -> {str(user.id_user)}\nНИК -> {user.nickname}\nБАЛАНС ->{str(user.balance)}\nДАТА РЕГИСТРАЦИИ {user.date_reg} "
        await m.answer(msg, reply_markup=user_menu(user.id_user))
    else:
        await m.answer('Пользователь не найден', reply_markup=admin())

@dp.callback_query_handler(change_balance.filter())
async def set_ammount(call:CallbackQuery,callback_data:dict,state:FSMContext):
    id_user = callback_data.get('id_user')
    await state.update_data(id_user = id_user)
    await call.message.edit_text('Введите новый баланс')
    await Donate.set_balance.set()

@dp.message_handler(state=Donate.set_balance)
async def finish_set(m:Message,state:FSMContext):
    data = await state.get_data()
    users = UserRepository(database)
    try:
        await users.update_balance_by_id(int(data['id_user']),float(m.text))
        await m.answer('Баланс изменен',reply_markup=admin())
    except:
        await m.answer('Неверно указанные данные, попробуйте еще раз')


@dp.callback_query_handler(text_contains='statistik')
async def get_user_for_id_m(call:CallbackQuery):
    payments = PayRepository(database)
    await call.message.edit_text(f'Всего было {str(len(await payments.get_all()))} покупок')
    await call.message.edit_reply_markup(admin())


