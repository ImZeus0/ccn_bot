from .main_handlers import dp
from .account_handlers import dp
from .search_handlers import dp
from .buy_handlers import dp
from .admin_handlers import dp

__all__ = ["dp"]