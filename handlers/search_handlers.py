from loader import bot, dp
from aiogram.types import Message
from aiogram.dispatcher import FSMContext
from keyboards import *
from state import *
from core import api
import datetime
from aiogram.types.callback_query import CallbackQuery


@dp.callback_query_handler(text_contains='seach')
async def start_seach(call: CallbackQuery):
    await call.message.edit_text('🔎 Поиск SSN по')
    await call.message.edit_reply_markup(seach_type())


@dp.callback_query_handler(text_contains='by_state')
async def seach_by_state_metod(call: CallbackQuery, state: FSMContext):
    await call.message.edit_text('Выберете штат')
    await state.update_data(type='state')
    await call.message.edit_reply_markup(states_kb())


@dp.callback_query_handler(text_contains='by_city')
async def seach_by_state_metod(call: CallbackQuery, state: FSMContext):
    await call.message.edit_text('Введите Город (en)')
    await state.update_data(type='city')
    await Input.city.set()


@dp.message_handler(state=Input.city)
async def input_city_metod(m: Message, state: FSMContext):
    await state.update_data(value=m.text)
    await m.answer("Введите Fist name")
    await Input.first_name.set()


@dp.callback_query_handler(text_contains='by_zip')
async def search_by_zip_metod(call: CallbackQuery, state: FSMContext):
    await call.message.edit_text('Введите ZIP ')
    await state.update_data(type='zip')
    await Input.zip.set()


@dp.message_handler(state=Input.zip)
async def input_zip_metod(m:Message,state:FSMContext):
    await m.answer("Введите Fist name")
    await state.update_data(value=m.text)
    await Input.first_name.set()



@dp.callback_query_handler(text_contains='by_dob')
async def search_by_dob_metod(call: CallbackQuery, state: FSMContext):
    await call.message.edit_text('Введите DOB в формате YYYYMMDD ')
    await state.update_data(type='dob')
    await Input.dob.set()


@dp.message_handler(state=Input.dob)
async def input_dob_metod(m:Message,state:FSMContext):
    await m.answer("Введите Fist name")
    await state.update_data(value=m.text)
    await Input.first_name.set()

@dp.callback_query_handler(choose_state.filter())
async def input_metod(call: CallbackQuery, callback_data: dict, state: FSMContext):
    value_state = callback_data.get('value')
    await call.message.edit_text("Введите Fist name")
    await state.update_data(value=value_state)
    await Input.first_name.set()


@dp.message_handler(state=Input.first_name)
async def f_name_metod(m: Message, state: FSMContext):
    await state.update_data(first_name=m.text)
    await m.answer('Введите Last name')
    await Input.last_name.set()


async def send_product(data,m:Message):

    if len(data) > 30:
        data = data[0:30]

    for product in data:
        text = '<b>🔸 SSN</b> '+product['ssn']+'\n'
        text += '<b>🔸 NAME</b> ' + product['name'] + '\n'
        text += '<b>🔸 ADDRESS</b> ' + product['address'] + '\n'
        text += '<b>🔸 DOB</b> ' + product['dob'] + '\n'
        text += '<b>🔸 DOB YEAR</b> ' + product['dob_year'] + '\n'
        await m.answer(text,reply_markup=buy(product['hid'],product['rid']))


@dp.message_handler(state=Input.last_name)
async def l_name_metod(m: Message, state: FSMContext):
    await state.update_data(last_name=m.text)
    data = await state.get_data()
    value = data['value']
    f_name = data['first_name']
    l_name = data['last_name']
    type = data['type']
    await m.answer(f"🔍 Поиск по {type}\n<b>{value} | {f_name} | {l_name}</b>\n\n⚙ Обработка запроса ...")
    responce = await api.seach(type,f_name,l_name,value)
    if responce['status'] == 4:
        await m.answer('❌ Не найдено результатов по запросу',reply_markup=start())
        await state.finish()
    elif responce['status'] == 0:
        await send_product(responce['data'],m)
    await m.answer('Конец поиска',reply_markup=end_search())



