from loader import bot, dp
from keyboards import *
from core.config import ADMINS
from repositories.pays import PayRepository
from models.orders import Order
from repositories.users import UserRepository
from db.base import database
from core import api
from aiogram.types.callback_query import CallbackQuery

@dp.callback_query_handler(buy_callback.filter())
async def buy_metod(call:CallbackQuery,callback_data:dict):
    hid = callback_data.get('hid')
    rid = callback_data.get('rid')
    price = float(callback_data.get('price'))
    users = UserRepository(database)
    user = await users.get_by_id(call.message.chat.id)
    if user.balance > price:
        await call.message.edit_text('⚙ Ваш заказ обрабатывается ...')
        pays = PayRepository(database)
        responce = await api.buy(hid,rid)
        if responce['status'] == 0:
            res_balance = user.balance - price
            await users.update_balance_by_id(call.message.chat.id,res_balance)
            product = responce['data']
            order = Order(buyer=user.id_user,
                          ssn = product['ssn'],
                          name=product['name'],
                          address=product['address'],
                          dob=product['dob'],
                          dob_year=product['dob_year'],
                          res_balance=res_balance)
            await pays.create_pay(order)
            text = '<b>Ваш заказ</b>\n\n'
            text += '<b>❇ SSN</b> ' + product['ssn'] + '\n'
            text += '<b>❇ NAME</b> ' + product['name'] + '\n'
            text += '<b>❇ ADDRESS</b> ' + product['address'] + '\n'
            text += '<b>❇ DOB</b> ' + product['dob'] + '\n'
            text += '<b>❇ DOB YEAR</b> ' + product['dob_year'] + '\n'
            await call.message.edit_text(text)
            await call.message.edit_reply_markup(back_mainmenu())
        if responce['status'] == 5:
            for id in ADMINS:
                await bot.send_message(id,'Нужно пополнить баланс')
                await call.answer('Ошибка сервиса, попробуйте немного позже')
    else:
        await call.message.edit_text('Нужно пополнить баланс')
        await call.message.edit_reply_markup(donate())

