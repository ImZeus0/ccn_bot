from loader import dp
from aiogram.types import Message
from aiogram.dispatcher import FSMContext
from keyboards import *
from repositories.users import UserRepository
from db.base import database
from aiogram.types.callback_query import CallbackQuery
from core import config


@dp.message_handler(commands=['start'],state='*')
async def mainstart(m:Message,state :FSMContext):
    await state.finish()
    users = UserRepository(database)
    args = m.text.split(' ')
    if await users.is_register(m.chat.id):
        if await users.is_admin(m.chat.id):
            await m.answer('🔹 Главное меню 🔹',reply_markup=start_admin())
        else:
            await m.answer('🔹 Главное меню 🔹',reply_markup=start())
    else:

        if len(args) > 1:
            await users.create(m.chat.id,m.chat.username,int(args[1]))
        else:
            await users.create(m.chat.id, m.chat.username, -1)
        await m.answer('🔹 Главное меню 🔹', reply_markup=start())


@dp.callback_query_handler(text_contains="mainmenu",state='*')
async def back(call:CallbackQuery,state :FSMContext):
    users = UserRepository(database)
    await state.finish()
    await call.message.edit_text('🔹 Главное меню 🔹')
    if await users.is_admin(call.message.chat.id):
        await call.message.edit_reply_markup(start_admin())
    else:
        await call.message.edit_reply_markup(start())

@dp.callback_query_handler(text_contains="rules")
async def show_rules(call:CallbackQuery):
    await call.message.edit_text(config.RULES)
    await call.message.edit_reply_markup(back_mainmenu())

@dp.callback_query_handler(text_contains="support")
async def show_support(call:CallbackQuery):
    await call.message.edit_text(config.SUPPORT)
    await call.message.edit_reply_markup(back_mainmenu())
